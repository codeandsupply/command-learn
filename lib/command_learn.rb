class CommandLearn
  PROJECT_SETTINGS_DIR = File.expand_path('projects', File.join(File.dirname(__FILE__),'..'))

  def install(project)
    status = false
    message = 'Could not install project'
    if settings = find_project_settings(project)
      status = message = install_project(settings)
    else
      message = 'Could not find a settings file'
    end
    {message: message, status: status}
  end

  def test(project)
    status = false
    message = 'Could not run tests'
    if settings = find_project_settings(project)
      status = message = run_tests(settings)
    else
      message = 'Could not find a settings file'
    end
    {message: message, status: status}
  end

  private
    def install_project(settings)
      if settings['install_from'] == 'here'
        source_dir_name = File.join(settings['settings_dir'], '.')
        install_dir_name = settings['install_dir_name']
        mkdir_p install_dir_name
        cp_r(source_dir_name, install_dir_name)
        return 'Installed'
      end
    end

    def settings_dir(name)
      if File.exists?('settings.json') || name.nil?
        pwd
      else
        File.join(PROJECT_SETTINGS_DIR, name)
      end
    end

    def install_dir(name)
      if File.exists?('settings.json') || name.nil?
        pwd
      else
        File.join(pwd, 'command-learn-projects', name)
      end
    end

    def settings_meta(name)
      settings_dir = settings_dir(name)
      install_dir = install_dir(name)
      file_name = File.join(settings_dir, 'settings.json')

      added_settings = {
        'settings_dir' => settings_dir,
        'name' => name,
        'install_dir_name' => install_dir,
        'settings_file_name' => file_name
      }
    end

    def find_project_settings(name)
      meta = settings_meta(name)
      file_name = meta['settings_file_name']

      if File.exist?(file_name)
        settings = JSON.parse(File.read(file_name))
        settings.merge(meta)
      else
        return false
      end
    end

    def run_tests(settings)
      cd settings['settings_dir']
      `#{settings['test_command']}`
    end
end
