require 'thor'
require 'json'
require 'fileutils'
require_relative 'command_learn'
require_relative 'cli_refinements'

using CLIRefinements
include FileUtils

module CLIUtils
  class CLIParser < Thor
    desc 'install PROJECT', 'Installs PROJECT at the current locations'
    option :project
    def install
      app = CommandLearn.new
      resp = app.install(options[:project])
      MessageDisplayer.new(resp).display
    end

    desc 'test PROJECT', 'Runs all of the tests for the PROJECT project'
    long_desc <<-LONGDESC
      Runs all of the tests on a project.

      With --project option, will run all tests for given project name
      With no project specified, will run project based on current directory
    LONGDESC
    option :project
    def test
      app = CommandLearn.new
      resp = app.test(options[:project])
      MessageDisplayer.new(resp).display
    end

    desc 'install PROJECT', 'Installs PROJECT at the current locations'
    alias :i :install
  end

  class MessageDisplayer
    using CLIRefinements
    attr_accessor :status, :message

    def initialize(response)
      @status = response[:status]
      @message = response[:message]
    end

    def display
      if @status
        puts @message.green
        return true
      else
        puts @message.red
        return false
      end
    end
  end

  def parse_options(args)
    CLIParser.start(args)
  end
end
