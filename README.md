Command Learn
=============

Command Learn is a tool to learn new programming languages through the command
line.

Install
=======

- Clone this repository
- Make the `command-learn` file executable with `chmod +x command-learn`

Use
===

Run the command `./command-learn` from this directory to get a list of commands.

By default Command Learn looks for a
`settings.json` file in the current directory or by
a given project name. If you run `./command-learn
--project ruby`, then Command Learn will search
`projects/ruby` for a `settings.json` file.

Commands
========

* install PROJECT - installs a project by name in the current directory
* test - runs all tests for the current project

Disclaimer
==========

```
Command Learn is a work in progress. Do not go any further.
```
